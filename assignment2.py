from flask import Flask, render_template, request, url_for, redirect, json, jsonify, Response
import random

app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
{'title': 'Algorithm Design', 'id':'2'}, \
{'title': 'Python', 'id':'3'}]

@app.route('/book/JSON')
def bookJSON():
	r = json.dumps(books)
	return r

@app.route('/')
@app.route('/book/')
def showBook():
	return render_template('showBook.html', books = books)

@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
	#check if the user input any book
	if request.method == 'POST':
		counter = 0
		#iterate through the books list and see if there's empty slot
		for i in range (0,len(books)):
			#if there is empty slot, put the new book on the slot
			if int(books[i]["id"]) != i+1:
				new_book = {"id" : str(i+1), "title": request.form["new_book"]}
				books.insert(i, new_book)
			#if not, append the new book to the list
			else:
				counter += 1
				if counter == len(books):
					new_book = {"id" : str(counter + 1), "title": request.form["new_book"]}
					books.append(new_book)
		#render the html
		return render_template('showBook.html', books=books)

	else:
		return render_template('newBook.html')

@app.route('/book/<int:book_id>/edit/', methods=['POST', 'GET'])
def editBook(book_id):
	#find the book information save it in a dictionary first
	for i in books:
		if int(i["id"]) == book_id:
			book = {'id' : str(book_id), 'title' : i['title']}
	#check if the user want to edit the book
	if request.method == 'POST':
		#if so, substitute the title of the book with the new name
		for i in books:
			if int(i["id"]) == book_id:
				i["title"] = request.form["new_name"]
		#render the template
		return render_template('showBook.html', books=books)
		
	else:
		return render_template('editBook.html', book_id = book_id, book = book)


@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
	#store the information of the book we want to delete first
	for i in books:
		if int(i["id"]) == book_id:
			book = {'id' : str(book_id), 'title' : i['title']}
	#check if the user want to delete the book
	if request.method == 'POST':
		#if so, remove the dictionary of the book from the list
		books[:] = [i for i in books if int(i["id"]) != book_id]
		#return the template
		return render_template('showBook.html', books = books)

	else:
		return render_template('deleteBook.html', book_id = book_id, book = book)




if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
